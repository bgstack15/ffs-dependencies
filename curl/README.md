# curl upstream
http://mirror.city-fan.org/ftp/contrib/yum-repo/development/source/curl-7.66.0-1.1.cf.fc32.src.rpm

# Reason for being in ffs-dependencies
FreeFileSync follows libcurl very aggressively.

# Reverse dependency matrix
Distro     | FreeFileSync version | libcurl version
---------- | -------------------- | ---------------
CentOS 7   | 10.16                | 7.66.0
CentOS 8   | 10.16                | 7.66.0

# Differences from upstream
No changes.
