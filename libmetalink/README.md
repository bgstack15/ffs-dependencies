# libmetalink upstream
On a CentOS 8 system run

    dnf download libmetalink --source

# Reason for being in ffs-dependencies
FreeFileSync follows libcurl, and to build libcurl you need libmetalink-devel.
CentOS 8 provides libmetalink, but does not provide libmetalink-devel as of 2019-10-10.

# Reverse dependency matrix
Distro     | FreeFileSync version | libmetalink version
---------- | -------------------- | ---------------
CentOS 8   | 10.16                | same as distro

# Differences from upstream
No changes.
