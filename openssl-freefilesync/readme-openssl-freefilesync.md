# Purpose of this package
// vim: syntax=markdown
Package openssl-freefilesync exists to enable the compilation and execution of FreeFileSync on CentOS 7. CentOS 7 does not bundle high-enough versions of openssl in the base repositories, so the bgstack15 copr serves a customized package of a sufficient version of openssl. The city-fan repo does not provide this one, so I have to hack the Fedora package myself.

# How to maintain openssl-freefilesync
Take a [Fedora release](https://mirrors.rit.edu/fedora/fedora/linux/updates/30/Everything/SRPMS/Packages/o/openssl-1.1.1c-2.fc30.src.rpm) of openssl and open it.

    rpm2cpio openssl-1.1.1c-2.fc30.src.rpm | cpio -idm

Make any modifications necessary to get it to compile. This changes include:
* Revert any Fedora improvements to rpm specs since CentOS 7 was released, such as `%ldconfig_scriptlets`
* Rename cnf and man pages and the main binary so they do not collide with system openssl package
