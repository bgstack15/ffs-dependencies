# openssl upstream
https://mirrors.rit.edu/fedora/fedora/linux/updates/30/Everything/SRPMS/Packages/o/openssl-1.1.1d-2.fc30.src.rpm

# Reason for being in ffs-dependencies
FreeFileSync follows openssl very aggressively.

# Reverse dependency matrix
Distro     | FreeFileSync version | openssl version
---------- | -------------------- | ---------------
CentOS 7   | 10.16                | 1.1.1

# Differences from upstream
See file [stackrpms-openssl.spec.diff](stackrpms-openssl.spec.diff)
