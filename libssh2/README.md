# libssh2 upstream
http://mirror.city-fan.org/ftp/contrib/yum-repo/rhel7/source/libssh2-1.9.0-2.0.cf.rhel7.src.rpm

# Reason for being in ffs-dependencies
FreeFileSync follows libssh2 very aggressively.

# Reverse dependency matrix
Distro     | FreeFileSync version | libssh2 version
---------- | -------------------- | ---------------
CentOS 7   | 10.16                | 1.9.0

# Differences from upstream
Disable tests in rpm spec, and rename package slightly.
See file [stackrpms-libssh2.spec.diff](stackrpms-libssh2.spec.diff)
