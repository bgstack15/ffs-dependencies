# Detect the distribution in use
%global __despace head -n 1 | tr -d '[:space:]' | sed -e 's/[(].*[)]//g'
%global __lower4 cut -c 1-4 | tr '[:upper:]' '[:lower:]'
%global __distfile %([ -f /etc/SuSE-release ] && echo /etc/SuSE-release || echo /etc/redhat-release)
%global __distinit %(sed -e 's/ release .*//' -e 's/\\([A-Za-z]\\)[^ ]*/\\1/g' %{__distfile} | %{__despace} | %{__lower4})
%global __distvers %(sed -e 's/.* release \\([^. ]*\\).*/\\1/' %{__distfile} | %{__despace})
# Identify CentOS Linux and Scientific Linux as rhel
%if "%{__distinit}" == "c" || "%{__distinit}" == "cl" || "%{__distinit}" == "sl" || "%{__distinit}" == "sls"
%global __distinit rhel
%endif
# Dist tag for Fedora is still "fc"
%if "%{__distinit}" == "f"
%global __distinit fc
%endif

# Define %%{__isa_bits} for old releases
%{!?__isa_bits: %global __isa_bits %((echo '#include <bits/wordsize.h>'; echo __WORDSIZE) | cpp - | grep -Ex '32|64')}

%global shortname libssh2
Name:		libssh2-freefilesync
Version:	1.9.0
Release:	3.0.stack.%{__distinit}%{__distvers}
Summary:	A library implementing the SSH2 protocol
License:	BSD
URL:		http://www.libssh2.org/
# stackrpms: use https for copr build
Source0:	https://libssh2.org/download/libssh2-%{version}.tar.gz
Patch0:		libssh2-1.7.0-pkgconfig.patch
BuildRequires:	coreutils
BuildRequires:	findutils
BuildRequires:	gcc
BuildRequires:	make
BuildRequires:	openssl-devel > 1:1.0.1
BuildRequires:	pkgconfig
BuildRequires:	sed
BuildRequires:	zlib-devel
BuildRequires:	/usr/bin/man
%if 0%{?fedora} > 20 || 0%{?rhel} > 7
BuildRequires:	hostname
%else
BuildRequires:	/bin/hostname
%endif
# OpenSSH server used in test suite
BuildRequires:	openssh-server
# Need a valid locale to run the mansyntax check
%if 0%{?fedora} > 23 || 0%{?rhel} > 7
BuildRequires:	glibc-langpack-en
%endif
# We use matchpathcon from libselinux-utils to get the correct SELinux context
# for the ssh server initialization script so that it can transition correctly
# in an SELinux environment
BuildRequires:	libselinux-utils
BuildRequires:	selinux-policy-targeted
Obsoletes: 	libssh2

%description
libssh2 is a library implementing the SSH2 protocol as defined by
Internet Drafts: SECSH-TRANS(22), SECSH-USERAUTH(25),
SECSH-CONNECTION(23), SECSH-ARCH(20), SECSH-FILEXFER(06)*,
SECSH-DHGEX(04), and SECSH-NUMBERS(10).

%package	devel
Summary:	Development files for libssh2
Requires:	%{name}%{?_isa} = %{version}-%{release}
Requires:	pkgconfig

%description	devel
The libssh2-devel package contains libraries and header files for
developing applications that use libssh2.

%package	docs
Summary:	Documentation for libssh2
Requires:	%{name} = %{version}-%{release}
BuildArch:	noarch

%description	docs
The libssh2-docs package contains man pages and examples for
developing applications that use libssh2.

%prep
%setup -q -n %{shortname}-%{version}

# Replace hard wired port number in the test suite to avoid collisions
# between 32-bit and 64-bit builds running on a single build-host
sed -i s/4711/47%{?__isa_bits}/ tests/ssh2.{c,sh}

# Fix pkg-config --libs output (#1279966)
%patch0

# Make sshd transition appropriately if building in an SELinux environment
chcon $(/usr/sbin/matchpathcon -n /etc/rc.d/init.d/sshd) tests/ssh2.sh || :
chcon -R $(/usr/sbin/matchpathcon -n /etc) tests/etc || :
chcon $(/usr/sbin/matchpathcon -n /etc/ssh/ssh_host_key) tests/etc/{host,user} || :

%build
export CPPFLAGS="$(pkg-config --cflags openssl)"
%configure --disable-silent-rules --disable-static --enable-shared
make %{?_smp_mflags}

%install
make install DESTDIR=%{buildroot} INSTALL="install -p"
find %{buildroot} -name '*.la' -delete

# clean things up a bit for packaging
make -C example clean
rm -rf example/.deps
find example/ -type f '(' -name '*.am' -o -name '*.in' ')' -delete

# avoid multilib conflict on libssh2-devel
mv -v example example.%{_arch}

%check
echo "Running tests for %{_arch}"
# The SSH test will fail if we don't have /dev/tty, as is the case in some
# versions of mock (#672713)
if [ ! -c /dev/tty ]; then
	echo Skipping SSH test due to missing /dev/tty
	echo "exit 0" > tests/ssh2.sh
fi
# Apparently it fails in the sparc and arm buildsystems too
%ifarch %{sparc} %{arm}
echo Skipping SSH test on sparc/arm
echo "exit 0" > tests/ssh2.sh
%endif
# mansyntax check fails on PPC* and aarch64 with some strange locale error
%ifarch ppc %{power64} aarch64
echo "Skipping mansyntax test on PPC* and aarch64"
echo "exit 0" > tests/mansyntax.sh
%endif
# stackrpms: disable tests for copr build
#LC_ALL=en_US.UTF-8 make -C tests check

%if (0%{?rhel} && 0%{?rhel} <= 7) || (0%{?fedora} && 0%{?fedora} <= 27)
# ldconfig replaced by RPM File Triggers from Fedora 28
%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig
%endif

%files
%if 0%{?_licensedir:1}
%license COPYING
%else
%doc COPYING
%endif
%doc docs/AUTHORS ChangeLog README RELEASE-NOTES
%{_libdir}/libssh2.so.1
%{_libdir}/libssh2.so.1.*

%files docs
%doc docs/BINDINGS docs/HACKING docs/TODO NEWS
%{_mandir}/man3/libssh2_*.3*

%files devel
%doc example.%{_arch}/
%{_includedir}/libssh2.h
%{_includedir}/libssh2_publickey.h
%{_includedir}/libssh2_sftp.h
%{_libdir}/libssh2.so
%{_libdir}/pkgconfig/libssh2.pc

%changelog
* Wed Oct 09 2019 Ben Stack <bgstack15@gmail.com> - 1.9.0-3.0.stack
- Disable tests so it will build in copr

* Tue Sep 17 2019 Ben Stack <bgstack15@gmail.com> - 1.9.0-2.0.stack
- Disable tests so it will build in copr

* Fri Jul 26 2019 Paul Howarth <paul@city-fan.org> - 1.9.0-2.0.cf
- Rebuilt for https://fedoraproject.org/wiki/Fedora_31_Mass_Rebuild

* Thu Jun 20 2019 Paul Howarth <paul@city-fan.org> - 1.9.0-1.0.cf
- Update to 1.9.0
  - Fixed integer overflow leading to out-of-bounds read (CVE-2019-13115)
  - Adds ECDSA keys and host key support when using OpenSSL
  - Adds ED25519 key and host key support when using OpenSSL 1.1.1
  - Adds OpenSSH style key file reading
  - Adds AES CTR mode support when using WinCNG
  - Adds PEM passphrase protected file support for libgcrypt and WinCNG
  - Adds SHA256 hostkey fingerprint
  - Adds libssh2_agent_get_identity_path() and libssh2_agent_set_identity_path()
  - Adds explicit zeroing of sensitive data in memory
  - Adds additional bounds checks to network buffer reads
  - Adds the ability to use the server default permissions when creating sftp directories
  - Adds support for building with OpenSSL no engine flag
  - Adds support for building with LibreSSL
  - Increased sftp packet size to 256k
  - Fixed oversized packet handling in sftp
  - Fixed building with OpenSSL 1.1
  - Fixed a possible crash if sftp stat gets an unexpected response
  - Fixed incorrect parsing of the KEX preference string value
  - Fixed conditional RSA and AES-CTR support
  - Fixed a small memory leak during the key exchange process
  - Fixed a possible memory leak of the ssh banner string
  - Fixed various small memory leaks in the backends
  - Fixed possible out of bounds read when parsing public keys from the server
  - Fixed possible out of bounds read when parsing invalid PEM files
  - No longer null terminates the scp remote exec command
  - Now handle errors when Diffie Hellman key pair generation fails
  - Fixed compiling on Windows with the flag STDCALL=ON
  - Improved building instructions
  - Improved unit tests
- Needs OpenSSL ≥ 1.0.1 now as ECC support is assumed

* Tue Mar 26 2019 Paul Howarth <paul@city-fan.org> - 1.8.2-1.0.cf
- Update to 1.8.2
  - Fixed the misapplied userauth patch that broke 1.8.1
  - Moved the MAX size declarations from the public header

* Tue Mar 19 2019 Paul Howarth <paul@city-fan.org> - 1.8.1-1.0.cf
- Update to 1.8.1
  - Fixed possible integer overflow when reading a specially crafted packet
    (CVE-2019-3855)
  - Fixed possible integer overflow in userauth_keyboard_interactive with a
    number of extremely long prompt strings (CVE-2019-3863)
  - Fixed possible integer overflow if the server sent an extremely large
    number of keyboard prompts (CVE-2019-3856)
  - Fixed possible out of bounds read when processing a specially crafted
    packet (CVE-2019-3861)
  - Fixed possible integer overflow when receiving a specially crafted exit
    signal message channel packet (CVE-2019-3857)
  - Fixed possible out of bounds read when receiving a specially crafted exit
    status message channel packet (CVE-2019-3862)
  - Fixed possible zero byte allocation when reading a specially crafted SFTP
    packet (CVE-2019-3858)
  - Fixed possible out of bounds reads when processing specially crafted SFTP
    packets (CVE-2019-3860)
  - Fixed possible out of bounds reads in _libssh2_packet_require(v)
    (CVE-2019-3859)
- Fix mis-applied patch in the fix of CVE-2019-3859
  - https://github.com/libssh2/libssh2/issues/325
  - https://github.com/libssh2/libssh2/pull/327

* Mon Feb  4 2019 Paul Howarth <paul@city-fan.org> - 1.8.0-10.0.cf
- Explicitly run the test suite in the en_US.UTF-8 locale to work around flaky
  locale settings in mock builders

* Fri Jul 13 2018 Paul Howarth <paul@city-fan.org> - 1.8.0-8.0.cf
- Rebuilt for https://fedoraproject.org/wiki/Fedora_29_Mass_Rebuild

* Wed Feb 14 2018 Paul Howarth <paul@city-fan.org> - 1.8.0-7.0.cf
- ldconfig replaced by RPM File Triggers from Fedora 28

* Tue Sep 12 2017 Paul Howarth <paul@city-fan.org> - 1.8.0-5.0.cf
- scp: Do not NUL-terminate the command for remote exec (#1489736, GH#208)
- Make devel package dependency on main package arch-specific

* Thu Jul 27 2017 Paul Howarth <paul@city-fan.org> - 1.8.0-4.0.cf
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Binutils_Mass_Rebuild

* Thu Jul 27 2017 Paul Howarth <paul@city-fan.org> - 1.8.0-3.0.cf
- Drop support for EOL distributions prior to F-13
  - Drop BuildRoot: and Group: tags
  - Drop explicit buildroot cleaning in %%install section
  - Drop explicit %%clean section
  - noarch sub-packages always available now
  - libselinux-utils always available now

* Sat Feb 11 2017 Paul Howarth <paul@city-fan.org> - 1.8.0-2.0.cf
- Rebuilt for https://fedoraproject.org/wiki/Fedora_26_Mass_Rebuild

* Tue Oct 25 2016 Paul Howarth <paul@city-fan.org> - 1.8.0-1.0.cf
- Update to 1.8.0
  - Added a basic dockerised test suite
  - crypto: Add support for the mbedTLS backend
  - libgcrypt: Fixed a NULL pointer dereference on OOM
  - VMS: Can't use %%zd for off_t format
  - VMS: Update vms/libssh2_config.h
  - Windows: Link with crypt32.lib
  - libssh2_channel_open: Speeling error fixed in channel error message
  - msvc: Fixed 14 compilation warnings
  - tests: HAVE_NETINET_IN_H was not defined correctly
  - openssl: Add OpenSSL 1.1.0 compatibility
  - cmake: Add CLEAR_MEMORY option, analogously to that for autoconf
  - configure: Make the --with-* options override the OpenSSL default
  - libssh2_wait_socket: Set err_msg on errors
  - libssh2_wait_socket: Fix comparison with api_timeout to use milliseconds

* Thu Oct 20 2016 Paul Howarth <paul@city-fan.org> - 1.7.0-7.0.cf
- Make curl test suite work again with valgrind enabled

* Wed Oct 12 2016 Paul Howarth <paul@city-fan.org> - 1.7.0-6.0.cf
- Include upstream fix for OpenSSL 1.1.0 compatibility

* Sun Mar  6 2016 Paul Howarth <paul@city-fan.org> - 1.7.0-5.0.cf
- Move large NEWS file to docs package
- Use -delete with find

* Wed Feb 24 2016 Paul Howarth <paul@city-fan.org> - 1.7.0-3.0.cf
- diffie_hellman_sha1: Convert bytes to bits (additional fix for CVE-2016-0787)
- Drop UTF-8 patch, which breaks things rather than fixes them

* Tue Feb 23 2016 Paul Howarth <paul@city-fan.org> - 1.7.0-1.0.cf
- Update to 1.7.0
  - diffie_hellman_sha256: convert bytes to bits (CVE-2016-0787); see
    http://www.libssh2.org/adv_20160223.html
  - libssh2_session_set_last_error: Add function
  - mac: Add support for HMAC-SHA-256 and HMAC-SHA-512
  - WinCNG: support for SHA256/512 HMAC
  - kex: Added diffie-hellman-group-exchange-sha256 support
  - OS/400 crypto library QC3 support
  - SFTP: Increase speed and datasize in SFTP read
  - openssl: Make libssh2_sha1 return error code
  - openssl: Fix memleak in _libssh2_dsa_sha1_verify()
  - cmake: Include CMake files in the release tarballs
  - Fix builds with Visual Studio 2015
  - hostkey.c: Fix compiling error when OPENSSL_NO_MD5 is defined
  - GNUmakefile: Add support for LIBSSH2_LDFLAG_EXTRAS
  - GNUmakefile: Add -m64 CFLAGS when targeting mingw64
  - kex: free server host key before allocating it (again)
  - SCP: Add libssh2_scp_recv2 to support large (> 2GB) files on windows
  - channel: Detect bad usage of libssh2_channel_process_startup
  - userauth: Fix off by one error when reading public key file
  - kex: Removed dupe entry from libssh2_kex_methods
  - _libssh2_error: Support allocating the error message
  - hostkey: Fix invalid memory access if libssh2_dsa_new fails
  - hostkey: Align code path of ssh_rsa_init to ssh_dss_init
  - libssh2.pc.in: Fix the output of pkg-config --libs
  - wincng: Fixed possible memory leak in _libssh2_wincng_hash
  - wincng: Fixed _libssh2_wincng_hash_final return value
  - Add OpenSSL 1.1.0-pre2 compatibility
  - agent_disconnect_unix: Unset the agent fd after closing it
  - sftp: Stop reading when buffer is full
  - sftp: Send at least one read request before reading
  - sftp: Don't return EAGAIN if data was written to buffer
  - sftp: Check read packet file offset
  - configure: build "silent" if possible
  - openssl: Add OpenSSL 1.1.0-pre3-dev compatibility
  - GNUmakefile: List system libs after user libs
- Update pkgconfig patch

* Thu Feb  4 2016 Paul Howarth <paul@city-fan.org> - 1.6.0-4.0.cf
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Tue Nov 10 2015 Paul Howarth <paul@city-fan.org> - 1.6.0-3.0.cf
- Fix pkg-config --libs output (#1279966)

* Thu Jun 18 2015 Paul Howarth <paul@city-fan.org> - 1.6.0-2.0.cf
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Sun Jun 14 2015 Paul Howarth <paul@city-fan.org> - 1.6.0-1.0.cf
- Update to 1.6.0
  - Added CMake build system
  - Added libssh2_userauth_publickey_frommemory()
  - wait_socket: Wrong use of difftime()
  - userauth: Fixed prompt text no longer being copied to the prompt's struct
  - mingw build: Allow to pass custom CFLAGS
  - Let mansyntax.sh work regardless of where it is called from
  - Init HMAC_CTX before using it
  - direct_tcpip: Fixed channel write
  - WinCNG: Fixed backend breakage
  - OpenSSL: Fix bug caused by introducing libssh2_hmac_ctx_init
  - userauth.c: Fix possible dereferences of a null pointer
  - wincng: Added explicit clear memory feature to WinCNG backend
  - openssl.c: Fix possible segfault in case EVP_DigestInit fails
  - wincng: Fix return code of libssh2_md5_init()
  - kex: Do not ignore failure of libssh2_sha1_init()
  - scp: Fix that scp_send may transmit uninitialized memory
  - scp.c: Improved command length calculation
  - nonblocking examples: Fix warning about unused tvdiff on Mac OS X
  - configure: Make clear-memory default but WARN if backend unsupported
  - OpenSSL: Enable use of OpenSSL that doesn't have DSA
  - OpenSSL: Use correct no-blowfish #define
  - kex: Fix libgcrypt memory leaks of bignum
  - libssh2_channel_open: More detailed error message
  - wincng: Fixed memleak in (block) cipher destructor

* Wed Mar 11 2015 Paul Howarth <paul@city-fan.org> - 1.5.0-1.0.cf
- Update to 1.5.0
  - Added Windows Cryptography API: Next Generation based backend
  - Security Advisory for CVE-2015-1782, using SSH_MSG_KEXINIT data unbounded
  - Missing _libssh2_error in _libssh2_channel_write
  - knownhost: Fix DSS keys being detected as unknown
  - knownhost: Restore behaviour of 'libssh2_knownhost_writeline' with short
    buffer
  - libssh2.h: On Windows, a socket is of type SOCKET, not int
  - libssh2_priv.h: A 1 bit bit-field should be unsigned
  - Windows build: Do not export externals from static library
  - Fixed two potential use-after-frees of the payload buffer
  - Fixed a few memory leaks in error paths
  - userauth: Fixed an attempt to free from stack on error
  - agent_list_identities: Fixed memory leak on OOM
  - knownhosts: Abort if the hosts buffer is too small
  - sftp_close_handle: Ensure the handle is always closed
  - channel_close: Close the channel even in the case of errors
  - Docs: Added missing libssh2_session_handshake.3 file
  - Docs: Fixed a bunch of typos
  - userauth_password: Pass on the underlying error code
  - _libssh2_channel_forward_cancel: Accessed struct after free
  - _libssh2_packet_add: Avoid using uninitialized memory
  - _libssh2_channel_forward_cancel: Avoid memory leaks on error
  - _libssh2_channel_write: Client spins on write when window full
  - Windows build: Fix build errors
  - publickey_packet_receive: Avoid junk in returned pointers
  - channel_receive_window_adjust: Store windows size always
  - userauth_hostbased_fromfile: Zero assign to avoid uninitialized use
  - configure: Change LIBS not LDFLAGS when checking for libs
  - agent_connect_unix: Make sure there's a trailing zero
  - MinGW build: Fixed redefine warnings
  - sftpdir.c: Added authentication method detection
  - Watcom build: Added support for WinCNG build
  - configure.ac: Replace AM_CONFIG_HEADER with AC_CONFIG_HEADERS
  - sftp_statvfs: Fix for servers not supporting statfvs extension
  - knownhost.c: Use LIBSSH2_FREE macro instead of free
  - Fixed compilation using mingw-w64
  - knownhost.c: Fixed that 'key_type_len' may be used uninitialized
  - configure: Display individual crypto backends on separate lines
  - Examples on Windows: Check for WSAStartup return code
  - Examples on Windows: Check for socket return code
  - agent.c: Check return code of MapViewOfFile
  - kex.c: Fix possible NULL pointer de-reference with session->kex
  - packet.c: Fix possible NULL pointer de-reference within listen_state
  - Tests on Windows: Check for WSAStartup return code
  - userauth.c: Improve readability and clarity of for-loops
  - Examples on Windows: Use native SOCKET-type instead of int
  - packet.c: i < 256 was always true and i would overflow to 0
  - kex.c: Make sure mlist is not set to NULL
  - session.c: Check return value of session_nonblock in debug mode
  - session.c: Check return value of session_nonblock during startup
  - userauth.c: Make sure that sp_len is positive and avoid overflows
  - knownhost.c: Fix use of uninitialized argument variable wrote
  - openssl: Initialise the digest context before calling EVP_DigestInit()
  - libssh2_agent_init: Init ->fd to LIBSSH2_INVALID_SOCKET
  - configure.ac: Add zlib to Requires.private in libssh2.pc if using zlib
  - configure.ac: Rework crypto library detection
  - configure.ac: Reorder --with-* options in --help output
  - configure.ac: Call zlib zlib and not libz in text but keep option names
  - Fix non-autotools builds: Always define the LIBSSH2_OPENSSL CPP macro
  - sftp: seek: Don't flush buffers on same offset
  - sftp: statvfs: Along error path, reset the correct 'state' variable
  - sftp: Add support for fsync (OpenSSH extension)
  - _libssh2_channel_read: Fix data drop when out of window
  - comp_method_zlib_decomp: Improve buffer growing algorithm
  - _libssh2_channel_read: Honour window_size_initial
  - window_size: Redid window handling for flow control reasons
  - knownhosts: Handle unknown key types

* Fri Oct 10 2014 Paul Howarth <paul@city-fan.org> - 1.4.3-16.0.cf
- Prevent a not-connected agent from closing STDIN (#1147717)

* Sun Aug 17 2014 Paul Howarth <paul@city-fan.org> - 1.4.3-15.0.cf
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_22_Mass_Rebuild

* Sat Jul 19 2014 Paul Howarth <paul@city-fan.org> - 1.4.3-14.0.cf
- Use %%license where possible

* Sun Jun  8 2014 Paul Howarth <paul@city-fan.org> - 1.4.3-13.0.cf
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Wed Apr 30 2014 Paul Howarth <paul@city-fan.org> - 1.4.3-12.0.cf
- Fix curl's excessive memory consumption during scp download
- BR: hostname package rather than /bin/hostname from F-21 as the latter is no
  longer provided in rawhide
- Drop %%defattr, redundant since rpm 4.4

* Mon Feb 17 2014 Paul Howarth <paul@city-fan.org> - 1.4.3-11.0.cf
- Skip the manpage syntax check on ppc* and aarch64 as there are wierd locale
  issues in their buildroots

* Wed Aug 14 2013 Paul Howarth <paul@city-fan.org> - 1.4.3-8.0.cf
- Fix very slow sftp upload to localhost
- Fix a use after free in channel.c

* Sat Aug  3 2013 Paul Howarth <paul@city-fan.org> - 1.4.3-7.0.cf
- Rebuilt for https://fedoraproject.org/wiki/Fedora_20_Mass_Rebuild

* Tue Apr  9 2013 Paul Howarth <paul@city-fan.org> - 1.4.3-6.0.cf
- Add three patches from upstream git required for qemu ssh block driver

* Wed Apr  3 2013 Paul Howarth <paul@city-fan.org> - 1.4.3-4.0.cf
- Avoid polluting libssh2.pc with linker options (#947813)

* Tue Mar 26 2013 Paul Howarth <paul@city-fan.org> - 1.4.3-3.0.cf
- Avoid collisions between 32-bit and 64-bit builds running on a single
  build-host

* Thu Feb 14 2013 Paul Howarth <paul@city-fan.org> - 1.4.3-2.0.cf
- Rebuilt for https://fedoraproject.org/wiki/Fedora_19_Mass_Rebuild

* Wed Nov 28 2012 Paul Howarth <paul@city-fan.org> - 1.4.3-1.0.cf
- Update to 1.4.3
  - compression: add support for zlib@openssh.com
  - sftp_read: return error if a too large package arrives
  - libssh2_hostkey_hash.3: update the description of return value
  - Fixed MSVC NMakefile
  - examples: use stderr for messages, stdout for data
  - openssl: do not leak memory when handling errors
  - improved handling of disabled MD5 algorithm in OpenSSL
  - known_hosts: Fail when parsing unknown keys in known_hosts file
  - configure: gcrypt doesn't come with pkg-config support
  - session_free: wrong variable used for keeping state
  - libssh2_userauth_publickey_fromfile_ex.3: mention publickey == NULL
  - comp_method_zlib_decomp: handle Z_BUF_ERROR when inflating
- Drop upstreamed patches

* Wed Nov  7 2012 Paul Howarth <paul@city-fan.org> - 1.4.2-4.0.cf
- examples: use stderr for messages, stdout for data (upstream commit b31e35ab)
- Update libssh2_hostkey_hash(3) man page (upstream commit fe8f3deb)

* Mon Oct  1 2012 Paul Howarth <paul@city-fan.org> - 1.4.2-3.0.cf
- Fix basic functionality of libssh2 in FIPS mode

* Thu Jul 19 2012 Paul Howarth <paul@city-fan.org> - 1.4.2-2.0.cf
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Sun May 20 2012 Paul Howarth <paul@city-fan.org> - 1.4.2-1.0.cf
- Update to 1.4.2
  - Return LIBSSH2_ERROR_SOCKET_DISCONNECT on EOF when reading banner
  - userauth.c: fread() from public key file to correctly detect any errors
  - configure.ac: add option to disable build of the example applications
  - added 'Requires.private:' line to libssh2.pc
  - SFTP: filter off incoming "zombie" responses
  - gettimeofday: no need for a replacement under cygwin
  - SSH_MSG_CHANNEL_REQUEST: default to want_reply
  - win32/libssh2_config.h: remove hardcoded #define LIBSSH2_HAVE_ZLIB

* Fri Apr 27 2012 Paul Howarth <paul@city-fan.org> - 1.4.1-2.0.cf
- Fix multi-arch conflict again (#816969)

* Thu Apr  5 2012 Paul Howarth <paul@city-fan.org> - 1.4.1-1.0.cf
- Update to 1.4.1
  - Build error with gcrypt backend
  - Always do "forced" window updates to avoid corner case stalls
  - aes: the init function fails when OpenSSL has AES support
  - transport_send: finish in-progress key exchange before sending data
  - channel_write: acknowledge transport errors
  - examples/x11.c: make sure sizeof passed to read operation is correct
  - examples/x11.c: fix suspicious sizeof usage
  - sftp_packet_add: verify the packet before accepting it
  - SFTP: preserve the original error code more
  - sftp_packet_read: adjust window size as necessary
  - Use safer snprintf rather then sprintf in several places
  - Define and use LIBSSH2_INVALID_SOCKET instead of INVALID_SOCKET
  - sftp_write: cannot return acked data *and* EAGAIN
  - sftp_read: avoid data *and* EAGAIN
  - libssh2.h: add missing prototype for libssh2_session_banner_set()
- Drop upstream patches now included in release tarball

* Mon Mar 19 2012 Paul Howarth <paul@city-fan.org> - 1.4.0-4.0.cf
- Don't ignore transport errors when writing to channel (#804150)

* Sun Mar 18 2012 Paul Howarth <paul@city-fan.org> - 1.4.0-3.0.cf
- Don't try to use openssl's AES-CTR functions
  (http://www.libssh2.org/mail/libssh2-devel-archive-2012-03/0111.shtml)

* Fri Mar 16 2012 Paul Howarth <paul@city-fan.org> - 1.4.0-2.0.cf
- Fix libssh2 failing key re-exchange when write channel is saturated (#804156)
- Drop support for distributions prior to FC-3:
  - BR: openssh-server unconditionally

* Wed Feb  1 2012 Paul Howarth <paul@city-fan.org> - 1.4.0-1.0.cf
- Update to 1.4.0
  - Added libssh2_session_supported_algs()
  - Added libssh2_session_banner_get()
  - Added libssh2_sftp_get_channel()
  - libssh2.h: bump the default window size to 256K
  - sftp-seek: clear EOF flag
  - userauth: provide more informations if ssh pub key extraction fails
  - ssh2_exec: skip error outputs for EAGAIN
  - LIBSSH2_SFTP_PACKET_MAXLEN: increase to 80000
  - knownhost_check(): don't dereference ext if NULL is passed
  - knownhost_add: avoid dereferencing uninitialized memory on error path
  - OpenSSL EVP: fix threaded use of structs
  - _libssh2_channel_read: react on errors from receive_window_adjust
  - sftp_read: cap the read ahead maximum amount
  - _libssh2_channel_read: fix non-blocking window adjusting
- Add upstream patch fixing undefined function reference in libgcrypt backend
- BR: /usr/bin/man for test suite

* Mon Jan 16 2012 Paul Howarth <paul@city-fan.org> - 1.3.0-4.0.cf
- Skip the ssh test on sparc and arm buildsystems, where it tends to fail

* Fri Jan 13 2012 Paul Howarth <paul@city-fan.org> - 1.3.0-3.0.cf
- Example includes arch-specific bits, so move to devel package

* Wed Sep  7 2011 Paul Howarth <paul@city-fan.org> - 1.3.0-1.0.cf
- Update to 1.3.0
  - Added custom callbacks for performing low level socket I/O
  - sftp_read: advance offset correctly for buffered copies
  - libssh2_sftp_seek64: flush packetlist and buffered data
  - _libssh2_packet_add: adjust window size when truncating
  - sftp_read: a short read is not end of file

* Tue Aug 16 2011 Paul Howarth <paul@city-fan.org> - 1.2.9-1.0.cf
- Update to 1.2.9
  - Added libssh2_session_set_timeout() and libssh2_session_get_timeout() to
    make blocking calls get a timeout
  - configure and pkg-config: fix $VERSION
  - s/\.NF/.nf/ to fix wrong macro name caught by man --warnings
  - keepalive: add first basic man pages
  - sftp_write: flush the packetlist on error
  - sftp_write: clean offsets on error
  - msvcproj: added libs and debug stuff
  - SCP: fix incorrect error code
  - session_startup: init state properly
  - sftp_write_sliding: send the complete file
  - userauth_keyboard_interactive: skip code on zero length auth
  - _libssh2_wait_socket: fix timeouts for poll() uses
  - agent_list_identities: fix out of scope access
  - _libssh2_recv(): handle ENOENT error as EAGAIN
  - userauth_keyboard_interactive: fix buffer overflow
  - removed man pages for non-existing functions!
  - gettimeofday: fix name space pollution
  - _libssh2_channel_write: handle window_size == 0 better
- Drop upstreamed version patch
- Use patch rather than scripted iconv to fix character encoding
- Fix dist tag for CentOS 6 and Scientific Linux

* Wed Apr  6 2011 Paul Howarth <paul@city-fan.org> - 1.2.8-1.0.cf
- Update to 1.2.8
  - Added libssh2_free, libssh2_channel_get_exit_signal and
    libssh2_session_handshake
  - SFTP read/write remade and now MUCH faster, especially on high latency
    connections
  - Added new examples: ssh2_echo.c, sftp_append.c and sftp_write_sliding.c
  - userauth: derive publickey from private
  - Support unlimited number of host names in a single line of the
    known_hosts file
  - Fix memory leak in userauth_keyboard_interactive()
  - Fix memory leaks (two times cipher_data) for each sftp session
  - session_startup: manage server data before server identification
  - SCP: allow file names with bytes > 126
  - scp_recv: improved treatment of channel_read() returning zero
  - libssh2_userauth_authenticated: make it work as documented
  - variable size cleanup: match internal variable sizes better with the sizes
    of the fields used on the wire
  - channel_request_pty_size: fix reqPTY_state
  - sftp_symlink: return error if receive buffer too small
  - sftp_readdir: return error if buffer is too small
  - libssh2_knownhost_readfile.3: clarify return value
  - configure: stop using the deprecated AM_INIT_AUTOMAKE syntax
  - Fixed Win32 makefile which was now broken at resource build
  - kex_agree_hostkey: fix NULL pointer derefence
  - _libssh2_ntohu64: fix conversion from network bytes to uint64
  - ssize_t: proper typedef with MSVC compilers
  - zlib: Add debug tracing of zlib errors
  - decomp: increase decompression buffer sizes
- Skip the SSH test if we don't have /dev/tty (#672713)
- Nobody else likes macros for commands

* Wed Oct 13 2010 Paul Howarth <paul@city-fan.org> - 1.2.7-1.1.cf
- Improve support for running tests with SELinux in enforcing mode
- Avoid multilib conflict on libssh2-docs

* Wed Aug 18 2010 Paul Howarth <paul@city-fan.org> - 1.2.7-1.0.cf
- Update to 1.2.7
  - Better handling of invalid key files
  - inputchecks: make lots of API functions check for NULL pointers
  - libssh2_session_callback_set: extended the man page
  - SFTP: limit write() to not produce overly large packets
  - agent: make libssh2_agent_userauth() work blocking properly
  - _libssh2_userauth_publickey: reject method names longer than the data
  - channel_free: ignore problems with channel_close()
  - typedef: make ssize_t get typedef without LIBSSH2_WIN32
  - _libssh2_wait_socket: poll needs milliseconds
  - libssh2_wait_socket: reset error code to "leak" EAGAIN less
  - Added include for sys/select.h to get fd.set on some platforms
  - session_free: free more data to avoid memory leaks
  - openssl: make use of the EVP interface
  - Make libssh2_debug() create a correctly terminated string
  - userauth_hostbased_fromfile: packet length too short
  - handshake: Compression enabled at the wrong time
  - Don't overflow MD5 server hostkey

* Thu Jun 10 2010 Paul Howarth <paul@city-fan.org> - 1.2.6-1.0.cf
- Update to 1.2.6
  - Added libssh2_sftp_statvfs() and libssh2_sftp_fstatvfs()
  - Added libssh2_knownhost_checkp()
  - Added libssh2_scp_send64()
  - wait_socket made c89 compliant and use two fd_sets for select()
  - OpenSSL AES-128-CTR detection fixed
  - Proper keyboard-interactive user dialog in the sftp.c example
  - Fixed Build procedure for VMS
  - Fixed libssh2.dsw to use the generated libssh2.dsp
  - Several Windows-related build fixes
  - Fixed fail to init SFTP if session isn't already authenticated
  - Many tiny fixes that address clang-analyzer warnings
  - sftp_open: deal with short channel_write calls
  - libssh2_publickey_init: fixed to work better non-blocking
  - sftp_close_handle: add precation to not access NULL pointer
  - sftp_readdir: simplified and bugfixed
  - channel_write: if data has been sent, don't return EAGAIN
- Drop upstreamed AES-CTR detection patch

* Tue Jun  8 2010 Paul Howarth <paul@city-fan.org> - 1.2.5-3.1.cf
- RHEL-6 has noarch subpackages, so use one for docs

* Tue May 25 2010 Paul Howarth <paul@city-fan.org> - 1.2.5-3.0.cf
- Fix dist tag for RHEL-6 Beta

* Wed Apr 28 2010 Paul Howarth <paul@city-fan.org> - 1.2.5-2.0.cf
- Add buildreq openssh-server to enable additional test coverage
- Make sshd transition appropriately if building in an SELinux environment
  (a mock buildroot is not such an environment)
- Add buildreq /usr/sbin/matchpathcon to get appropriate SELinux context

* Wed Apr 14 2010 Paul Howarth <paul@city-fan.org> - 1.2.5-1.0.cf
- Update to 1.2.5
  - Add keep-alive support: libssh2_keepalive_config()/libssh2_keepalive_send()
  - Add libssh2_knownhost_addc(), libssh2_init() and libssh2_exit()
  - Add LIBSSH2_SFTP_S_IS***() macros
  - Fix memory leak in libssh2_session_startup()
  - Add missing error codes - shown as hangs in blocking mode
  - Fix memory leak in userauth_keyboard_interactive()
  - libssh2_knownhost_del: fix write to freed memory
  - Send and receive channel EOF before sending SSH_MSG_CHANNEL_CLOSE
  - Use AES-CTR from OpenSSL when available
  - Fixed gettimeofday to compile with Visual C++ 6
  - NULL dereference when window adjusting a non-existing channel
  - Avoid using poll on interix and mac os x systems
  - Fix scp memory leak
  - Correctly clear blocking flag after sending multipart packet
  - Reduce used window sizes by factor 10
  - libssh2_userauth_publickey_fromfile_ex() handles a NULL password
  - sftp_init() deal with _libssh2_channel_write() short returns
- Add patch to fix AES-CTR detection
- Add buildreq /bin/hostname for build host detection

* Mon Feb 15 2010 Paul Howarth <paul@city-fan.org> - 1.2.4-1.0.cf
- Update to 1.2.4
  - Resolve compile issues on Solaris x64 and UltraSPARC
  - Allow compiling with OpenSSL when AES isn't available
  - Fix Tru64 socklen_t compile issue with example/direct_tcpip.c

* Wed Feb  3 2010 Paul Howarth <paul@city-fan.org> - 1.2.3-1.0.cf
- Update to 1.2.3
  - Add libssh2_trace_sethandler()
  - Add the direct_tcpip.c example
  - Fix memory leak in userauth_publickey
  - Add support for authentication via SSH-Agent
  - Fix OpenSSH server keepalive (see NEWS)
- Drop padding patch

* Fri Jan 22 2010 Paul Howarth <paul@city-fan.org> - 1.2.2-5.0.cf
- Rename padding patch as per Fedora package
- sshd tests intentionally avoided (by not having openssh-server buildreq)
  because they appear to hang in the buildsystem

* Thu Dec 24 2009 Paul Howarth <paul@city-fan.org> - 1.2.2-2.0.cf
- Fix padding in ssh-dss signature blob encoding (#539444)

* Tue Nov 17 2009 Paul Howarth <paul@city-fan.org> - 1.2.2-1.0.cf
- Update to 1.2.2
  - fix crash when server sends an invalid SSH_MSG_IGNORE message
  - support for aes128-ctr, aes192-ctr, aes256-ctr ciphers as per RFC 4344
  - support for arcfour128 cipher as per RFC 4345
- Dist tag for Rawhide no longer needs special-casing
- Make docs package noarch where possible

* Wed Sep 30 2009 Paul Howarth <paul@city-fan.org> - 1.2.1-1.0.cf
- Update to 1.2.1 (many bugfixes - see NEWS)
- Drop upstreamed transport layer patch
- Devel package now includes a pkgconfig file (and requires pkgconfig)
- Don't redundantly include COPYING in docs and devel packages

* Wed Sep 23 2009 Paul Howarth <paul@city-fan.org> - 1.2-2.0.cf
- Update to 1.2 (see RELEASE-NOTES for details)
- Upstream source moved to libssh2.org
- Add upstream patch to fix transport layer bug causing invalid -39
  (LIBSSH2_ERROR_BAD_USE) errors

* Thu Apr  2 2009 Paul Howarth <paul@city-fan.org> - 1.1-1.0.cf
- Update to 1.1 (fixes memory leak and boosts sftp performance)

* Mon Jan 26 2009 Paul Howarth <paul@city-fan.org> - 1.0-1.0.cf
- Update to 1.0
- Help the configure script find openssl by setting CPPFLAGS

* Fri Aug  1 2008 Paul Howarth <paul@city-fan.org> - 0.18-9.0.cf
- Tweak dist tag macros to work on current Rawhide with three-part releasenum

* Fri Jul  4 2008 Paul Howarth <paul@city-fan.org> - 0.18-7.0.cf
- Import from Fedora
- Fix find syntax
- Add buildreq pkgconfig, missing dep from openssl-devel in older distributions

* Mon Feb 18 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 0.18-7
- Autorebuild for GCC 4.3

* Wed Dec 05 2007 Chris Weyl <cweyl@alumni.drew.edu> 0.18-6
- rebuild for new openssl...

* Tue Nov 27 2007 Chris Weyl <cweyl@alumni.drew.edu> 0.18-5
- bump

* Tue Nov 27 2007 Chris Weyl <cweyl@alumni.drew.edu> 0.18-4
- add INSTALL arg to make install vs env. var

* Mon Nov 26 2007 Chris Weyl <cweyl@alumni.drew.edu> 0.18-3
- run tests; don't package test

* Sun Nov 18 2007 Chris Weyl <cweyl@alumni.drew.edu> 0.18-2
- split docs into -docs (they seemed... large.)

* Tue Nov 13 2007 Chris Weyl <cweyl@alumni.drew.edu> 0.18-1
- update to 0.18

* Sun Oct 14 2007 Chris Weyl <cweyl@alumni.drew.edu> 0.17-1
- update to 0.17
- many spec file changes

* Wed May 23 2007 Sindre Pedersen Bjørdal <foolish[AT]guezz.net> - 0.15-0.2.20070506
- Fix release tag
- Move manpages to -devel package
- Add Examples dir to -devel package

* Sun May 06 2007 Sindre Pedersen Bjørdal <foolish[AT]guezz.net> - 0.15-0.20070506.1
- Initial build
