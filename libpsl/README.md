# libpsl upstream
On a CentOS 8 system run

    dnf download libpsl --source

# Reason for being in ffs-dependencies:
FreeFileSync follows libcurl, and to build libcurl you need libpsl-devel.
CentOS 8 provides libpsl, but does not provide libpsl-devel as of 2019-10-10.

# Reverse dependency matrix
Distro     | FreeFileSync version | libpsl version
---------- | -------------------- | ---------------
CentOS 8   | 10.16                | same as distro

# Differences from upstream
No changes.
